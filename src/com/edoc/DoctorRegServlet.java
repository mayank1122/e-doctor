package com.edoc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;


import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DoctorRegServlet
 */
public class DoctorRegServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoctorRegServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
try {
			
			// Registering Driver
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String uid_d=request.getParameter("userid_d");
		String fname_d=request.getParameter("firstname");
		String lname_d=request.getParameter("lastname");
		String specs=request.getParameter("speciality");
		String pass_d=request.getParameter("password_d");
		String dd=request.getParameter("dob");
		String gen_d=request.getParameter("sex");
		String add_d=request.getParameter("message");
		String num_d=request.getParameter("phone_no");
		String email_d=request.getParameter("email");
		
		try {
		    
		     Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe","system","oracle");
			 PreparedStatement ps=con.prepareStatement("insert into doctors values(?,?,?,?,?,?,?,?,?,?,null)");
			 ps.setString(1, uid_d);
			 ps.setString(2, fname_d);
			 ps.setString(3, lname_d);
			 ps.setString(4, specs);
			 ps.setString(5, pass_d);
			 ps.setString(6, dd);
			 ps.setString(7, gen_d);
			 ps.setString(8, add_d);
			 ps.setString(9, num_d);
			 ps.setString(10, email_d);
			 int some=ps.executeUpdate();
			 if(some!=0){
				 
				response.sendRedirect("login_doctor.html"); 
				 
				 
			 }
			 con.close();
			 
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}

	}

}
