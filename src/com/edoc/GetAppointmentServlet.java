package com.edoc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class GetAppointmentServlet
 */
public class GetAppointmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAppointmentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
try {
			
			// Registering Driver
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
          String name=request.getParameter("nameforappointment");
		
		HttpSession s5= request.getSession();
		String pidi2=(String) s5.getAttribute("pidi");
		try {
		    
		     Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe","system","oracle");
		 	PreparedStatement ps1=con.prepareStatement("select fname from patients where userid=?");
			ps1.setString(1, pidi2);
			ResultSet rs1=ps1.executeQuery();
		    rs1.next();
		    String a=rs1.getString("fname");
			 PreparedStatement ps2=con.prepareStatement("update doctors set appointments=? where fname=?");
			 ps2.setString(1, a);
			 ps2.setString(2, name);
			 
			 
			 int some=ps2.executeUpdate();
			 if(some!=0){
				 
				response.sendRedirect("home.html"); 
				 
				 
			 }
			 con.close();
			 
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
		}
	}

}
