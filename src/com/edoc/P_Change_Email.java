package com.edoc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class P_Change_Email
 */
public class P_Change_Email extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public P_Change_Email() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
try {
			
			// Registering Driver
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
String new_email=request.getParameter("p_new_email");
		
		HttpSession s1= request.getSession();
		String pi=(String) s1.getAttribute("pid");
		try {
		    
		     Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe","system","oracle");
			 PreparedStatement ps=con.prepareStatement("update patients set email=? where userid=?");
			 ps.setString(1, new_email);
			 ps.setString(2, pi);
			 
			 
			 int some=ps.executeUpdate();
			 if(some!=0){
				 
				response.sendRedirect("logout_patient.html"); 
				 
				 
			 }
			 con.close();
			 
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
	}

}
